# Exercice : Audit du plaidoyer Simplon

## Consignes

Vous allez devoir réaliser un audit du [site du plaidoyer Simplon](https://plaidoyer.simplon.co/).  
Pour cela, vous allez pouvoir vous baser sur [la prez éco-conception que vous avez déjà eue](https://slides.com/ldevernay/eco-conception#/).  
Ici, on est plutôt sur de l'optimisation technique (site statique). On attend donc en retour :

-   vos retours sur le site proprement dit
-   une liste de préconisations par ordre de priorité
-   un document .md décrivant votre démarche d'audit
-   quelle stack technique auriez-vous préconisée?
    L'éco-conception, c'est top, mais n'oubliez pas de jeter un oeil à l'accessibilité et à la performance (ça vous aidera aussi à prioriser).

## Démarche d'audit

### Outils utilisés

-   Google Lighthouse
-   Outils de développement Firefox
-   EcoIndex
-   Wappalyzer
-   WAVE Evaluation Tool
-   Web Developer Checklist

### Processus d'audit

Je commence par analyser le site avec chacun des outils d'audit listés et je réfléchis à des axes d'amélioration selon les résultats renvoyés.

Je regarde aussi le code du site afin de connaître plus précisémment sa structure et obtenir des informations qui ne sont pas données par les outils d'audit.

## Analyse

### 1. Performance

Le site est léger et se charge rapidement. Lighthouse mesure 1,628 KB de données transférées au total. La majorité de ces données proviennent des images (1,608 KB), le document lui-même ne pesant que 20 KB. Le site n'utilise pas de webfonts ce qui rend le site d'autant plus léger.

Le CSS et le JS sont directement injectés dans des balises style et script, ce qui empêche leur mise en cache et oblige à télécharger de nouveau tout le contenu des fichiers à chaque changement de page.

Les images de la partie "objectifs de développement durable" sont retaillées directement via le HTML, ce qui augmente leur poids. Le nombre d'images augmente aussi le nombre de requêtes.

**Préconisations :**

-   Retailler les images de la partie "objectifs de développement durable" pour réduire leur poids, voire fournir des versions vectorielles au format SVG
-   Mettre le CSS et le JS dans des fichiers à part pour permettre leur mise en cache
-   Utiliser les sprites CSS pour réduire le nombre de requêtes
-   Indéxer les couleurs sur les images

Ces tâches peuvent être automatisées grâce à des outils comme Gulp/Webpack/Grunt

### 2. Éco-conception

EcoIndex donne un bon score (B) au site. La stack technique est aussi légère : Wappalyzer ne détecte que le logiciel serveur utilisé (Nginx). Le site semble avoir été codé entièrement à la main et n'utilise pas de framework CSS.

### 3. Accessibilité

L'accessibilité du site est bonne : il utilise des attributs alt pour ses images et la hiérarchie des titres type h1 est bien organisée. Les contrastes sont suffisamment élevés et le site reste lisible en version monochrome. Le site n'abuse pas des balises div et utilise correctement les balises sémantiques pour organiser son contenu.

WAVE et Lighthouse détectent des attributs tabindex avec une valeur supérieure à 0, ce qui modifie l'ordre par défaut du cycle de navigation via la touche de tabulation. Cette modification peut être volontaire, mais il faut faire attention à ne pas rendre la navigation frustrante pour les utilisateurs de technologies d'assistance.

Lorsque l'écran se réduit et que le menu de navigation se replie, il n'est plus accessible via la touche de tabulation.

Le texte de l'élément "page suivante" est trop petit, ce qui peut gêner les personnes malvoyantes.

**Préconisations :**

-   Rendre le menu de navigation accessible via la touche de tabulation
-   Augmenter la taille de police pour l'élément "page suivante"

## 4. Responsive

Le site est responsive et s'adapte bien à toutes les tailles d'écran via les media-queries.

## Conclusion

Le site est bien conçu dans l'ensemble. Sa stack technique est légère et les problématiques d'accessibilité et d'éco-conception ont été prises en compte. Les quelques axes d'amélioration suggérés ne sont pas très coûteux à mettre en place.
